<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:19 AM
 */

namespace Pengo\PostCodes\Block\Adminhtml;

use Magento\Framework\Setup\SampleData\FixtureManager;

class Main extends \Magento\Backend\Block\Template
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface $messageManager *
     */
    protected $messageManager;
    /**
     * @var \Pengo\PostCodes\Model\PengoPostCodesFactory $postcodesModel
     */
    protected $pengoPostCodes;
    /**
     * @var \Magento\Framework\File\Csv $csvProcessor
     */
    protected $csvProcessor;

    /**
     * Main constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Pengo\PostCodes\Model\PengoPostCodes $postcodesModel
     * @param \Magento\Framework\File\Csv $csvProcessor
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Pengo\PostCodes\Model\PengoPostCodesFactory $pengoPostCodes,
        \Magento\Framework\File\Csv $csvProcessor
    )
    {
        $this->messageManager = $messageManager;
        $this->pengoPostCodes = $pengoPostCodes;
        $this->csvProcessor = $csvProcessor;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Block\Template|void
     * @throws \Exception
     */
    public function _prepareLayout() {
        /** @var \Pengo\PostCodes\Model\PengoPostCodes $postcode */
        $postcode = $this->pengoPostCodes->create();
        $dataToSave = ['d_codigo', 'd_asenta', 'd_mnpio', 'd_estado', 'd_ciudad', ];

        foreach ($_FILES as $file) {
            $rows = $this->csvProcessor->getData($file['tmp_name']);
            $header = array_shift($rows);
            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $column = strtolower($header[$key]);
                    if(in_array($column, $dataToSave)){
                        $data[$column] = $value;
                    }
                }
                $postcode->unsetData();
                $postcode->setData($data);
                $postcode->save();
            }
            $this->messageManager->addSuccess(__("File uploaded"));
        }
    }

    /**
     * @return string
     */
    public function getFormKey() {
        return $this->formKey->getFormKey();
    }
}