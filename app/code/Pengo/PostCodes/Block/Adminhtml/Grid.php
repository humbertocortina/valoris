<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:20 AM
 */

namespace Pengo\PostCodes\Block\Adminhtml;


class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_index_grid';
        $this->_blockGroup = 'Pengo_PostCodes';
        $this->_headerText = __('Post codes');
        $this->_addButtonLabel = __('Create New Postcode');
        parent::_construct();
    }
}