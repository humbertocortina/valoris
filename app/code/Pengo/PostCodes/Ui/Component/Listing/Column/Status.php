<?php

namespace Pengo\PostCodes\Ui\Component\Listing\Column;

class Status extends \Magento\Ui\Component\Listing\Columns\Column {

    protected $countryInformation;

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ){
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item['status'] = ($item['status'] == 1) ? "Activo" : "Inactivo";
            }
        }
        return $dataSource;
    }
}