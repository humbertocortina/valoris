var config = {
    map : {
        '*': {
            customerAddressPostCodes: 'Pengo_PostCodes/js/address/postcodes',
        }
    },

    paths: {
        'select2': 'Pengo_PostCodes/js/lib/select2/select2.full',
    },

    shim: {
        'select2': {
            deps: ['jquery'],

        }
    }

};