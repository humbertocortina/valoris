<?php
/**
 * Created by PhpStorm.
 * User: saulhernandez
 * Date: 25/01/19
 * Time: 02:31 PM
 */

namespace Pengo\PostCodes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\File\Csv;
use Magento\Framework\Setup\SampleData\Context as SampleDataContext;
use Magento\Framework\Setup\SampleData\FixtureManager;
use Magento\Framework\Module\Dir\Reader;

class InstallData implements InstallDataInterface
{
    /**
     * @var SampleDataContext
     */
    private $sampleDataContext;
    /**
     * @var Csv
     */
    private $csvReader;
    /**
     * @var \Magento\Framework\Setup\SampleData\FixtureManager
     */
    private $fixtureManager;
    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    private $moduleReader;

    /**
     * InstallData constructor.
     * @param SampleDataContext $sampleDataContext
     * @param Csv $csvReader
     * @param \Magento\Framework\Setup\SampleData\FixtureManager $fixtureManager
     * @param \Magento\Framework\Module\Dir\Reader $moduleReader
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        Csv $csvReader,
        FixtureManager $fixtureManager,
        Reader $moduleReader
    ){

        $this->sampleDataContext = $sampleDataContext;
        $this->csvReader = $csvReader;
        $this->fixtureManager = $fixtureManager;
        $this->moduleReader = $moduleReader;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context){
        try {
            $directory = $this->moduleReader->getModuleDir('view', 'Pengo_PostCodes');
            $postcodes = [
                $directory.'/adminhtml/dataSample/ags.csv', $directory.'/adminhtml/dataSample/bcn.csv',
                $directory.'/adminhtml/dataSample/bcs.csv', $directory.'/adminhtml/dataSample/cam.csv',
                $directory.'/adminhtml/dataSample/chh.csv', $directory.'/adminhtml/dataSample/chp.csv',
                $directory.'/adminhtml/dataSample/cmx.csv', $directory.'/adminhtml/dataSample/coa.csv',
                $directory.'/adminhtml/dataSample/col.csv', $directory.'/adminhtml/dataSample/dur.csv',
                $directory.'/adminhtml/dataSample/gro.csv', $directory.'/adminhtml/dataSample/gua.csv',
                $directory.'/adminhtml/dataSample/hid.csv', $directory.'/adminhtml/dataSample/jal.csv',
                $directory.'/adminhtml/dataSample/mex.csv', $directory.'/adminhtml/dataSample/mic.csv',
                $directory.'/adminhtml/dataSample/mor.csv', $directory.'/adminhtml/dataSample/nle.csv',
                $directory.'/adminhtml/dataSample/oax.csv', $directory.'/adminhtml/dataSample/pue.csv',
                $directory.'/adminhtml/dataSample/que.csv', $directory.'/adminhtml/dataSample/roo.csv',
                $directory.'/adminhtml/dataSample/sin.csv', $directory.'/adminhtml/dataSample/slp.csv',
                $directory.'/adminhtml/dataSample/son.csv', $directory.'/adminhtml/dataSample/tab.csv',
                $directory.'/adminhtml/dataSample/tam.csv', $directory.'/adminhtml/dataSample/tla.csv',
                $directory.'/adminhtml/dataSample/ver.csv', $directory.'/adminhtml/dataSample/yuc.csv',
                $directory.'/adminhtml/dataSample/zac.csv'
            ];
            $this->createCodes($postcodes);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

    /**
     * @param array $codeFixtures
     * @throws \Exception
     */
    public function createCodes(array $codeFixtures)
    {
        $objectManager = ObjectManager::getInstance();
        /** @var \Pengo\PostCodes\Model\PengoPostCodes $postalCode */
        $postalCode = $objectManager->create('\Pengo\PostCodes\Model\PengoPostCodes');
        $dataToSave = ['d_codigo', 'd_asenta', 'd_mnpio', 'd_estado', 'd_ciudad', ];
        foreach ($codeFixtures as $fileName) {
            $fileName = $this->fixtureManager->getFixture($fileName);
            $rows = $this->csvReader->getData($fileName);
            $header = array_shift($rows);
            $path = explode("/", $fileName);
            $indexName = count($path) - 1;
            print "\n Installing data : ". $path[$indexName];
            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $column = strtolower($header[$key]);
                    if(in_array($column, $dataToSave)){
                        $data[$column] = $value;
                    }
                }
                $postalCode->unsetData();
                $postalCode->setData($data);
                $postalCode->save();
            }
        }
    }



}