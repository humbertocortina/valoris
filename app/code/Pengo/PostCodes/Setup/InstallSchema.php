<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 12:20 AM
 */

namespace Pengo\PostCodes\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

use Magento\Eav\Model\Config;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        $installer->getConnection()->dropTable($installer->getTable('pengo_postcodes'));
        $table = $installer->getConnection()->newTable($installer->getTable('pengo_postcodes'));

        $colums = [
            "postcode_id" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                "size" => 10,
                "options" => ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                "comment" => "ID"
            ],
            "d_codigo" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                "size" => 5,
                "options" => ['nullable' => false],
                "comment" => "Código Postal"
            ],
            "d_asenta" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                "size" => 120,
                "options" => ['nullable' => false],
                "comment" => "Colonia"
            ],
            "d_estado" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                "size" => 120,
                "options" => ['nullable' => false],
                "comment" => "Estado"
            ],
            "d_ciudad" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                "size" => 120,
                "options" => ['nullable' => true],
                "comment" => "Ciudad"
            ],
            "d_mnpio" => [
                "type" => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                "size" => 120,
                "options" => ['nullable' => false],
                "comment" => "Municipio"
            ]
        ];
        foreach ($colums as $colum => $values){
            try {
                $table->addColumn($colum, $values["type"], $values["size"], $values["options"], $values["comment"]);
            } catch (\Zend_Db_Exception $e) {
                echo $e->getMessage();
                continue;
            }
        }
        try {
            $installer->getConnection()->createTable($table);
        } catch (\Zend_Db_Exception $e) {
            echo $e->getMessage();
        }
        $installer->getConnection()->addIndex(
            $installer->getTable('pengo_postcodes'),
            $setup->getIdxName(
                $installer->getTable('pengo_postcodes'),
                ['d_codigo', 'd_asenta', 'd_estado', 'd_ciudad', 'd_mnpio'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['d_codigo', 'd_asenta', 'd_estado', 'd_ciudad', 'd_mnpio'],
            \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
        );
        $installer->endSetup();
    }
}