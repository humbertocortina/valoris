<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:03 AM
 */

namespace Pengo\PostCodes\Model\ResourceModel\PengoPostCodes;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'postcode_id';
    protected $_eventPrefix = 'pengo_postcodes_post_collection';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Pengo\PostCodes\Model\PengoPostCodes', 'Pengo\PostCodes\Model\ResourceModel\PengoPostCodes');
    }

}