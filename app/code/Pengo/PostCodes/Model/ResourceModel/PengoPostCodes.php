<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:01 AM
 */

namespace Pengo\PostCodes\Model\ResourceModel;


class PengoPostCodes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init('pengo_postcodes', 'postcode_id');
    }

}
