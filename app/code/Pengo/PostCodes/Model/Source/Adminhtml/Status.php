<?php
/**
 * Status column options
 * 
 * # NOTICE OF LICENSE
 * This work is licensed under a ***Creative Commons Attribution-NonCommercial-
 * NoDerivs 3.0 Unported License*** http://creativecommons.org/licenses/by-nc-nd/3.0
 *
 * ## Authors
 * Pengo Development Team  
 * Iván Miranda @deivanmiranda
 */
namespace Pengo\PostCodes\Model\Source\Adminhtml;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {    
		$arr = $this->toArray();
		$ret = [];
		foreach ($arr as $key => $value) {
			$ret[] = [
				'value' => $key,
				'label' => $value
			];
		}
		return $ret;
    }

	public function toArray()
	{
		$choose = [
			'0' => 'Inactive',
			'1' => 'Active'
		];
		return $choose;
	}
}
