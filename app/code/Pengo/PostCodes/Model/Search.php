<?php

namespace Pengo\PostCodes\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\ObjectManagerInterface;

class Search extends \Magento\Framework\Model\AbstractModel {
    /**
     * @var \Magento\Framework\ObjectManagerInterface $objectManager
     */
    protected $objectManager;
    /**
     * @var \Pengo\PostCodes\Model\PengoPostCodes $pengoPostCodes
     */
    private $pengoPostCodes;

    protected $searchFields = ['d_codigo'];

    protected $textFields = ['d_asenta', 'd_ciudad', 'd_mnpio', 'd_estado'];

    protected $sortByAttribute = 'postcode_id';

    /**
     * Search constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Pengo\PostCodes\Model\PengoPostCodes $pengoPostCodes
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        PengoPostCodes $pengoPostCodes
    ) {
        $this->objectManager = $objectManager;
        $this->pengoPostCodes = $pengoPostCodes;
    }

    /**
     * @param $query
     * @param $page
     * @return array
     */
    public function postCodeCollection($query, $page){
        $searchFields = $this->searchFields;
        $conditions = [['like' => '%' . $query . '%']];
        $items = [];
        $i = 0;

        $collection = $this->pengoPostCodes->getCollection();
        $collection->addFieldToFilter($searchFields, [$conditions]);
        if($page) {
            $collection->setPageSize(11);
            $collection->setCurPage($page);
        }
        if($this->sortByAttribute){
            $collection->setOrder($this->sortByAttribute,'ASC');
        }

        foreach($collection as $item){
            $i++;
            $codigoPostal = $item->getData('d_codigo');
            $codigoTexto = $this->getItemText($item);

            $codeNest = [
                'text' => $codigoPostal,
                'children' => []
            ];
            $codigoItem = [
                'id' => $codigoPostal,
                'text' => $codigoTexto,
            ];
            $key = $this->validateExitsinArray($items, 'text' ,$codigoPostal);
            if ($key || ($key === 0 && count($items) === 1)){
              $items[$key]['children'][] = $codigoItem;
            } else {
             $codeNest['children'][] = $codigoItem;
             $items[] = $codeNest;
            }
        }
        $more = $i / 10;
        return [(int)$more ,$items];
    }

    /**
     * @param $item
     * @return string
     */
    private function getItemText($item){
        $fields = $this->textFields;
        $text = '';
        foreach($fields as $field){
            //if($item->getData($field)) {
                $text .= $item->getData($field) .', ';
            //}

        }
        return $text;
    }

    /**
     * @param $products
     * @param $field
     * @param $value
     * @return bool|int
     */
    private function validateExitsinArray($products, $field, $value)
    {
        foreach($products as $key => $product)
        {
            if ( $product[$field] === $value )
                return (int)$key;
        }
        return false;
    }


    /**
     * @param $query
     * @param $page
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getPostCodeCollection($query, $page){
        $searchFields = $this->searchFields;
        $conditions = [['like' => '%' . $query . '%']];
        $items = [];
        $i = 0;

        $collection = $this->pengoPostCodes->getCollection();
        $collection->addFieldToFilter($searchFields, [$conditions]);
        if($page) {
            $collection->setPageSize(11);
            $collection->setCurPage($page);
        }
        if($this->sortByAttribute){
            $collection->setOrder($this->sortByAttribute,'ASC');
        }


        return $collection;
    }

}