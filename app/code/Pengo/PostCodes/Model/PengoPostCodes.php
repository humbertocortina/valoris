<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 01:59 AM
 */

namespace Pengo\PostCodes\Model;


class PengoPostCodes
    extends \Magento\Framework\Model\AbstractModel
    implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'pengo_postcodes_pengopostcodes';

    protected $_cacheTag = 'pengo_postcodes_pengopostcodes';

    protected $_eventPrefix = 'pengo_postcodes_pengopostcodes';

    protected function _construct()
    {
        $this->_init('Pengo\PostCodes\Model\ResourceModel\PengoPostCodes');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}