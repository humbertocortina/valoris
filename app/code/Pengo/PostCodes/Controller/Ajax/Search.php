<?php


namespace Pengo\PostCodes\Controller\Ajax;

class Search extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $jsonHelper;

    protected $search;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Pengo\PostCodes\Model\Search $search
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Pengo\PostCodes\Model\Search $search
    ){

        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->search = $search;

        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {

            $id = $this->getRequest()->getParam('id');
            $query = $postcode = $this->getRequest()->getParam('q');
            $page = $this->getRequest()->getParam('page', "1");

            if($query) {
                $items = $this->search->postCodeCollection($query, $page);
            }

            if($query || $id){
                $response = [
                    'query'=> $query,
                    'more' => (bool)$items[0],
                    'page' => $page,
                    'items'=> $items[1]
                ];
                return $this->jsonResponse($response);
            };

            return $this->jsonResponse([]);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @param string $response
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}