<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:25 AM
 */

namespace Pengo\PostCodes\Controller\Adminhtml\Grid;


class Delete extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $postcodesModel;
    protected $postcodes;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Pengo\PostCodes\Model\PengoPostCodes $postcodesModel)
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->postcodes = $postcodesModel;
        return parent::__construct($context);
    }

    public function execute()
    {
        if ($this->getRequest()->getParam('excluded')) {
            $postcodes = $this->postcodes->getCollection();
            $data = $postcodes->addFieldToFilter('postcode_id',  array('nin' => $this->getRequest()->getParam('excluded')));
            foreach ($data as $_postcodes) {
                $route = $this->postcodesModel->load($_postcodes->getId());
                $route->delete();
            }
        } else {
            foreach ($this->getRequest()->getParam('selected') as $id) {
                $route = $this->postcodesModel->load($id);
                $route->delete();
            }
        }
        $this->messageManager->addSuccess(__('Postcode(s) deleted'));
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('postcodes/grid/index');
        return $resultRedirect;
    }

}