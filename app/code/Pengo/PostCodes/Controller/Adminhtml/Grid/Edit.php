<?php
/**
 * Created by PhpStorm.
 * User: sul
 * Date: 23/01/19
 * Time: 02:25 AM
 */

namespace Pengo\PostCodes\Controller\Adminhtml\Grid;


class Edit extends \Magento\Backend\App\Action
{
    protected $resultJsonFactory;
    protected $postcodesModel;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Pengo\PostCodes\Model\PengoPostCodes $postcodesModel,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->postcodesModel = $postcodesModel;
        return parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $data = $this->getRequest()->getParam('items');
        $data = array_pop($data);
        $postcode = $this->postcodesModel->load($data['postcode_id'])->save();
        return $resultJson->setData([
            'messages' => [__('Data saved.')],
            'error' => false,
        ]);
    }
}
