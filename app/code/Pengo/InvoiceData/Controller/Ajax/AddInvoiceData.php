<?php


namespace Pengo\InvoiceData\Controller\Ajax;

class AddInvoiceData extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $jsonHelper;

    protected $_quoteFactory;

    protected $_cart;
    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Magento\Checkout\Model\Cart $cart
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->_quoteFactory = $quoteFactory;
        $this->_cart = $cart;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {

          $quote = $this->_cart->getQuote();
          $quoteId = $quote->getId();


            $require_invoice = $this->getRequest()->getParam('require_invoice');
            $business_name = $this->getRequest()->getParam('business_name');
            $rfc = $this->getRequest()->getParam('rfc');
            $tax_regime = $this->getRequest()->getParam('tax_regime');
            $email = $this->getRequest()->getParam('email');


            $quoteModel = $this->_quoteFactory->create()->loadByIdWithoutStore($quoteId);
            $quoteModel->setRequireInvoice(1);
            if(!empty($tax_regime)){
              $quoteModel->setTaxRegime($tax_regime);
            }
            if(!empty($rfc)){
              $quoteModel->setRfc($rfc);
            }
            if(!empty($business_name)){
              $quoteModel->setBusinessName($business_name);
            }
            if(!empty($email)){
              $quoteModel->setEmail($email);
            }

            $quoteModel->save();

            return $this->jsonResponse(array('msg' => 'ok'));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse(array('msg' => 'ko', 'error1' => $e->getMessage()));
        } catch (\Exception $e) {
            return $this->jsonResponse(array('msg' => 'ko', 'error2' => $e->getMessage()));
        }
    }

    /**
     * Create json response
     *
     * @param string $response
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
