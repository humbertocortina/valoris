<?php


namespace Pengo\InvoiceData\Setup;

use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    private $quoteSetupFactory;
    private $salesSetupFactory;

    /**
      * Constructor
      *
      * @param \Magento\Quote\Setup\QuoteSetupFactory $quoteSetupFactory
      */
     public function __construct(
         QuoteSetupFactory $quoteSetupFactory,
         SalesSetupFactory $salesSetupFactory
     ) {
         $this->quoteSetupFactory = $quoteSetupFactory;

         $this->salesSetupFactory = $salesSetupFactory;
     }

     /**
      * {@inheritdoc}
      */
     public function install(
         ModuleDataSetupInterface $setup,
         ModuleContextInterface $context
     ) {
         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         $quoteSetup->addAttribute('quote', 'require_invoice',
             [
                 'type' => 'varchar',
                 'length' => 1,
                 'visible' => false,
                 'required' => false,
                 'grid' => false
             ]
         );

         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         $quoteSetup->addAttribute('quote', 'tax_regime',
             [
                 'type' => 'varchar',
                 'length' => 1,
                 'visible' => false,
                 'required' => false,
                 'grid' => false
             ]
         );

         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         $quoteSetup->addAttribute('quote', 'rfc',
             [
                 'type' => 'varchar',
                 'length' => 16,
                 'visible' => false,
                 'required' => false,
                 'grid' => true
             ]
         );

         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         $quoteSetup->addAttribute('quote', 'business_name',
             [
                 'type' => 'varchar',
                 'length' => 120,
                 'visible' => false,
                 'required' => false,
                 'grid' => true
             ]
         );

         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         $quoteSetup->addAttribute('quote', 'email',
             [
                 'type' => 'varchar',
                 'length' => 80,
                 'visible' => false,
                 'required' => false,
                 'grid' => true
             ]
         );

         $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
         $salesSetup->addAttribute('order', 'require_invoice',
             [
                 'type' => 'varchar',
                 'length' => 1,
                 'visible' => false,
                 'required' => false,
                 'grid' => true
             ]
         );

         $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
         $salesSetup->addAttribute('order', 'tax_regime',
             [
                 'type' => 'varchar',
                 'length' => 1,
                 'visible' => false,
                 'required' => false,
                 'grid' => false
             ]
         );

         $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
         $salesSetup->addAttribute('order', 'rfc',
             [
                 'type' => 'varchar',
                 'length' => 16,
                 'visible' => false,
                 'required' => false,
                 'grid' => false
             ]
         );

         $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
         $salesSetup->addAttribute('order', 'business_name',
             [
                 'type' => 'varchar',
                 'length' => 120,
                 'visible' => false,
                 'required' => false,
                 'grid' => false
             ]
         );

          $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
          $salesSetup->addAttribute('order', 'email',
              [
                  'type' => 'varchar',
                  'length' => 80,
                  'visible' => false,
                  'required' => false,
                  'grid' => false
              ]
          );
     }
 }
