<?php
namespace Pengo\InvoiceData\Plugin\Checkout\Model\Checkout;

class LayoutProcessor
{
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ){

        $jsLayout = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'customCheckoutForm',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
            ],
            'provider' => 'checkoutProvider',
            'dataScope' => 'customCheckoutForm.text_field',
            'label' => 'Text Field',
            'sortOrder' => 1,
            'validation' => [
                'required-entry' => true,
            ],
        ];


        return $jsLayout;
    }
}
