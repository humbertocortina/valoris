<?php
namespace PracticsBS\SyncOpbMagento\Model\Resource;
use PracticsBS\SyncOpbMagento\Api\WebServiceRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\ResourceConnectionFactory;
/**
 * Class WebServiceRepository
 * @package PracticsBS\SyncOpbMagento\Model
 */
class WebServiceRepository implements WebServiceRepositoryInterface
{
    /**
     * @var ResourceConnectionFactory
     */
    protected $_resourceConnection;
    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollection;
    /**
     * @var CategoryFactory
     */
    protected $_category;
    /**
     * WebServiceRepository constructor.
     *
     * @param ResourceConnectionFactory $_resourceConnection
     */
    public function __construct(ResourceConnectionFactory $_resourceConnection, ProductCollectionFactory $_productCollection, CategoryFactory $_category)
    {
        $this->_resourceConnection = $_resourceConnection;
        $this->_productCollection = $_productCollection;
        $this->_category = $_category;
    }

    /**
     * @param $productId
     *
     * @return int
     */
    public function getProductParent($productId)
    {
        $size = 0;

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($productId);
        if(isset($product[0])){
             //this is parent product id..
             $size = $product[0];
        }
        return $size;
    }    
}