<?php
namespace PracticsBS\SyncOpbMagento\Api;
/**
 * Interface WebServiceRepositoryInterface
 * @package PracticsBS\SyncOpbMagento\Api
 */
interface WebServiceRepositoryInterface
{
    /**
     * @param int $productId
     *
     * @return mixed
     */
    public function getProductParent($productId);    
}