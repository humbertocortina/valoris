rm -rf pub/static/* generated/code/* var/cache/* var/page_cache/* var/view_preprocessed/*
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy -f en_US es_MX
chmod -R 777 var/ generated/ pub/
echo 'READY';
