/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'ko',
    'jquery',
    '../../lib/select2/select2.full',
    'Magento_Checkout/js/model/quote',
], function (_, registry, Abstract, ko, $, select2, quote) {
    'use strict';

    ko.bindingHandlers.select2 = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
            var $element = $(element);
            var options = ko.unwrap(valueAccessor());

            if (options.minimumInputLength){
                options.minimumInputLength = parseInt(options.minimumInputLength);
            }

            if (options.maximumInputLength){
                options.maximumInputLength = parseInt(options.maximumInputLength);
            }

            if (options.ajax){
                var ajaxOptions = {
                    ajax: {
                        url: "/define_url_in_xml",
                        dataType: 'json',
                        delay: 0,
                        type: 'GET',
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page,
                                form_key: window.FORM_KEY
                            };
                        },
                        processResults: function (data, params) {
                            params.page = params.page || 1;
                            return {
                                results: data.items,
                                pagination: {
                                    more: data.more
                                }
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: options.minimumInputLength ? options.minimumInputLength : 1
                };

                var searchType = (typeof options.ajax.search === 'undefined') ? '' : '/search/' + options.ajax.search;

                ajaxOptions.ajax.url = options.ajax.url + searchType;
                options = $.extend(options,ajaxOptions);

            }

            options.language = {
                errorLoading: function () {
                    return "No se pudieron cargar los resultados"
                }, inputTooLong: function (e) {
                    var t = e.input.length - e.maximum, n = "Por favor, elimine " + t + " car";
                    return t == 1 ? n += "ácter" : n += "acteres", n
                }, inputTooShort: function (e) {
                    var t = e.minimum - e.input.length, n = "Por favor, introduzca " + t + " car";
                    return t == 1 ? n += "ácter" : n += "acteres", n
                }, loadingMore: function () {
                    return "Cargando más resultados…"
                }, maximumSelected: function (e) {
                    var t = "Sólo puede seleccionar " + e.maximum + " elemento";
                    return e.maximum != 1 && (t += "s"), t
                }, noResults: function () {
                    return "No se encontraron resultados"
                }, searching: function () {
                    return "Buscando…"
                }
            };

            $element.select2(options);

            $element.on("select2:select", function (e) {});

            $element.on("select2:unselect", function (e) {});

        }
    };

    return Abstract.extend({

        defaults: {
            select2: {}
        },
        initialize: function () {
            this._super();
            this.quote = quote;
            this.shippingAddress = quote.shippingAddress;
            return this;
        },

        initObservable: function () {
            this._super();
            this.observe('select2');
            return this;
        },

        normalizeData: function (value) {

            this.getCurrentValue(value);

            return value;
        },

        getCurrentValue: function(value){

            if(value && this.select2().ajax) {
                var self = this;

                $.post(this.select2().ajax.url, { id: value, form_key: window.FORM_KEY},function (data) {
                    self.addCurrentValueToOptions(data.items, value);
                });
            }
        },

        addCurrentValueToOptions: function(items,value){
            var options;
            options = [];
            $.each(items, function(key,item) {
                options.push({'label': item.id, 'labeltitle': item.id, 'value': item.id});
            });
            this.setOptions(options);
            if(value) {
                this.value(value);
            }

        },

        getPreview: function () {
            var value = this.value(),
                option = this.indexedOptions[value],
                preview = option ? option.label : '';

            this.preview(preview);

            return preview;
        },

        /* Preview fix for use in filters */
        change: function(att, event){
            var option, values, cityUi, suburbUi, townUi, stateUi;

            var $element = $(event.target);
            if(this.select2().ajax) {
                var items = [];
                var value = $element.val();

                if(value) {
                    var label = $element.find("option[value="+value+"]").text();
                    items.push({'text':label,'id':value});
                }
                if (this.value()){
                    values = this.value.split(', ');
                    cityUi = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.city");
                    suburbUi = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.suburb");
                    townUi = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.town");
                    stateUi = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.region_id");

                    if (suburbUi && Boolean(values[0])) {
                        suburbUi.value(values[0]);
                        suburbUi.disabled(true);
                    } else{
                        suburbUi.value("");
                        suburbUi.disabled(false);
                    };

                    if (cityUi && Boolean(values[1])) {
                        cityUi.value(values[1]);
                        cityUi.disabled(true);
                    } else{
                        cityUi.value("");
                        cityUi.disabled(false);
                    };

                    if (townUi && Boolean(values[2])) {
                        townUi.value(values[2]);
                        townUi.disabled(true);
                    } else{
                        townUi.value("");
                        townUi.disabled(false);
                    };

                    if (stateUi && Boolean(values[3])){
                        if (stateUi.elementTmpl === "ui/form/element/select"){
                            var options = stateUi.options();
                            var valuePostCode = values[3];
                            if (valuePostCode === 'México' ||
                                valuePostCode === 'Ciudad de México' ||
                                valuePostCode === 'Veracruz de Ignacio de la Llave' ||
                                valuePostCode === 'Coahuila de Zaragoza'
                            ){
                                if (valuePostCode === 'México') valuePostCode = "Estado de México";
                                if (valuePostCode === 'Ciudad de México') valuePostCode =  "Distrito Federal";
                                if (valuePostCode === 'Veracruz de Ignacio de la Llave') valuePostCode =  "Veracruz";
                                if (valuePostCode === 'Coahuila de Zaragoza') valuePostCode =  "Coahuila";
                            }

                            for(var i = 0; i < options.length; i++){
                                var labelOption = options[i].label;
                                if (labelOption === valuePostCode){
                                    stateUi.value(options[i].value);
                                    break;
                                }
                            }
                        } else {
                            stateUi.value(values[3]);
                        }
                        stateUi.disabled(true);
                    } else {
                        stateUi.value("");
                        stateUi.disabled(false);
                    };
                }
                this.addCurrentValueToOptions(items,value);
            }
        }

    });
});
