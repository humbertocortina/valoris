define(
    [
        'ko',
        'Magento_Ui/js/form/form',
        'underscore',
        'Magento_Checkout/js/model/step-navigator',
        'jquery',
        'Magento_Ui/js/lib/validation/validator',
        'mage/url',
        'Magento_Ui/js/model/messageList'
    ],
    function (
        ko,
        Component,
        _,
        stepNavigator,
        $,
        validator,
        url,
        messageList
    ) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Pengo_InvoiceData/invoicedata'
            },

            //add here your logic to display step,
            isVisible: ko.observable(false),


            initialize: function () {
                this._super();
                stepNavigator.registerStep(
                    //step code will be used as step content id in the component template
                    'invoice',
                    //step alias
                    'invoice',
                    //step title value
                    'Facturación',
                    //observable property with logic when display step or hide step
                    this.isVisible,

                    _.bind(this.navigate, this),

                    /**
                     * sort order value
                     * 'sort order value' < 10: step displays before shipping step;
                     * 10 < 'sort order value' < 20 : step displays between shipping and payment step
                     * 'sort order value' > 20 : step displays after payment step
                     */
                    15
                );

                validator.addRule(
                    'validate-rfc',
                    function (rfcStr, element) {
                      var strCorrecta;
                    	strCorrecta = rfcStr;
                    	if (rfcStr.length == 12){
                    	var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
                    	}else{
                    	var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
                    	}
                    	var validRfc=new RegExp(valid);
                    	var matchArray=strCorrecta.match(validRfc);
                    	if (matchArray==null) {
                    		console.log('Cadena incorrectas');
                    		return false;
                    	}else{
                    		console.log('Cadena correcta:' + strCorrecta);
                    		return true;
                    	}
                    },
                    $.mage.__('El RFC no es incorrecto')
                );
                return this;
            },

            navigate: function () {
                var self = this;
                    self.isVisible(true);
            },

            navigateToNextStep: function () {

                this.source.set('params.invalid', false);
                this.source.trigger('customCheckoutForm.data.validate');
                if (!this.source.get('params.invalid')) {
                    var formData = this.source.get('customCheckoutForm');
                    console.log('Pasando información a quote');
                    console.dir('REQUIRE FACTURA : '+formData.require_invoice);
                    console.dir(formData);

                    $.ajax({
                          url: url.build('invoice/ajax/addinvoicedata/'),
                          dataType: 'json',
                          type : 'post',
                          async: true,
                          data: {
                              require_invoice:formData.require_invoice,
                              rfc:formData.rfc,
                              email:formData.email,
                              tax_regime:formData.tax_regime,
                              business_name:formData.business_name
                          },
                          success: function(data){
                            if(data.msg == 'ok'){
                              stepNavigator.next();
                            }else{
                              console.log('Error al insertar la información');
                              messageList.addErrorMessage({ message: 'Error al insertar la información.' });
                            }
                          }
                     });
                }else{
                  var formData = this.source.get('customCheckoutForm');
                  if(formData.require_invoice){
                    console.log('Si requiere factura, pero faltan campos');
                  }else{
                    console.log('No requiere factura, y todo pasara ok');
                    stepNavigator.next();
                  }
                }

            }
        });
    }
);
