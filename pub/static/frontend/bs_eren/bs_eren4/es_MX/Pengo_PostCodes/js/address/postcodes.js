/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'select2',
    'mage/translate',
    'jquery/ui',
    'mage/url',
], function ($) {
    'use strict';


    $.widget('pengo.postCodes', {
        options: {
            dropdownParent: $('div.field.zip.required > div.control'),
            placeholder: $.mage.__('Select a postal code'),
            width: "100%",
            minimumInputLength: 5,
            maximumInputLength: 5,
            ajax: {
                url: url.build('/postcodes/ajax/search/'),
                dataType: 'json',
                delay: 250,
                type: 'POST',
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page,
                        form_key: window.FORM_KEY
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: data.more
                        }
                    };
                },
                cache: false
            },
            language : {
                errorLoading: function () {
                    return "No se pudieron cargar los resultados"
                }, inputTooLong: function (e) {
                    var t = e.input.length - e.maximum, n = "Por favor, elimine " + t + " car";
                    return t == 1 ? n += "ácter" : n += "acteres", n
                }, inputTooShort: function (e) {
                    var t = e.minimum - e.input.length, n = "Por favor, introduzca " + t + " car";
                    return t == 1 ? n += "ácter" : n += "acteres", n
                }, loadingMore: function () {
                    return "Cargando más resultados…"
                }, maximumSelected: function (e) {
                    var t = "Sólo puede seleccionar " + e.maximum + " elemento";
                    return e.maximum != 1 && (t += "s"), t
                }, noResults: function () {
                    return "No se encontraron resultados"
                }, searching: function () {
                    return "Buscando…"
                }
            }
        },

        /** @inheritdoc */
        _create: function () {
            this.element.select2(this.options);
            this._bindSubmit();
            if (this.options.postcodeAddress){
                this.addPostcodeAddress();
            }
        },

        /**
         * @private
         */
        _bindSubmit: function () {
            var values, city, suburb, town, state,stateId;
            city    = $('input#city');
            suburb  = $('input#suburb');
            town    = $('input#town');
            stateId = $('select#region_id');
            state   = $('input#region');
            this.element.on('change', function (e) {
                if (window.checkoutConfig.select2Options && Boolean(this.value)){
                    values = window.checkoutConfig.select2Options.split(', ');
                    suburb.val("");
                    suburb.prop("readonly", false);
                    city.val("");
                    city.prop("readonly", false);
                    town.val("");
                    town.prop("readonly", false);

                    if (stateId.is(":visible")){
                        stateId.val("");
                    }

                    if(state.is(":visible")) {
                        state.val("");
                        state.prop("readonly", false);
                    }

                    if (suburb && Boolean(values[0])) {
                        suburb.val(values[0]);
                        suburb.prop("readonly", true);
                    }

                    if (city && Boolean(values[1])) {
                        city.val(values[1]);
                        city.prop("readonly", true);
                    }

                    if (town && Boolean(values[2])) {
                        town.val(values[2]);
                        town.prop("readonly", true);
                    }

                    if (Boolean(values[3])){
                        if (stateId.is(":visible")){
                            var options = stateId[0].options;
                            var valuePostCode = values[3];
                            if (valuePostCode === 'México' ||
                                valuePostCode === 'Ciudad de México' ||
                                valuePostCode === 'Veracruz de Ignacio de la Llave' ||
                                valuePostCode === 'Coahuila de Zaragoza'
                            ){
                                if (valuePostCode === 'México') valuePostCode = "Estado de México";
                                if (valuePostCode === 'Ciudad de México') valuePostCode =  "Distrito Federal";
                                if (valuePostCode === 'Veracruz de Ignacio de la Llave') valuePostCode =  "Veracruz";
                                if (valuePostCode === 'Coahuila de Zaragoza') valuePostCode =  "Coahuila";
                            }

                            for(var i = 0; i < options.length; i++){
                                var labelOption = options[i].label;
                                if (labelOption === valuePostCode){
                                    stateId.val(options[i].value);
                                    break;
                                }
                            }
                        }

                        if(state.is(":visible")) {
                            state.val(values[3]);
                            state.prop("readonly", true);
                        }
                    }
                }
                else if (window.checkoutConfig.select2Options && !Boolean(this.value)) {
                    suburb.val("");
                    suburb.prop("readonly", false);

                    city.val("");
                    city.prop("readonly", false);

                    town.val("");
                    town.prop("readonly", false);

                    if (stateId.is(":visible")){
                        stateId.val("");
                    }

                    if(state.is(":visible")) {
                        state.val("");
                        state.prop("readonly", false);
                    }

                }
            });
        },

        addPostcodeAddress: function () {
            var newOption = new Option(this.options.postcodeAddress, this.options.postcodeAddress, true, true);
            $(this.element).append(newOption).trigger('change');
        }
    });

    return $.pengo.postCodes;
});
