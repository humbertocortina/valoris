<?php
namespace Pengo\PostCodes\Controller\Adminhtml\Grid\Edit;

/**
 * Interceptor class for @see \Pengo\PostCodes\Controller\Adminhtml\Grid\Edit
 */
class Interceptor extends \Pengo\PostCodes\Controller\Adminhtml\Grid\Edit implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Pengo\PostCodes\Model\PengoPostCodes $postcodesModel, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->___init();
        parent::__construct($context, $postcodesModel, $resultJsonFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
