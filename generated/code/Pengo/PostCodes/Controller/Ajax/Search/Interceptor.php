<?php
namespace Pengo\PostCodes\Controller\Ajax\Search;

/**
 * Interceptor class for @see \Pengo\PostCodes\Controller\Ajax\Search
 */
class Interceptor extends \Pengo\PostCodes\Controller\Ajax\Search implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Json\Helper\Data $jsonHelper, \Pengo\PostCodes\Model\Search $search)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $jsonHelper, $search);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
