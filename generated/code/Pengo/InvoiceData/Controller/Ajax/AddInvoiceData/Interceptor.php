<?php
namespace Pengo\InvoiceData\Controller\Ajax\AddInvoiceData;

/**
 * Interceptor class for @see \Pengo\InvoiceData\Controller\Ajax\AddInvoiceData
 */
class Interceptor extends \Pengo\InvoiceData\Controller\Ajax\AddInvoiceData implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\Quote\Model\QuoteFactory $quoteFactory, \Magento\Checkout\Model\Cart $cart)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $jsonHelper, $quoteFactory, $cart);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
