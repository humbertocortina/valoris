<?php
namespace Magento\Quote\Api\Data;

/**
 * Extension class for @see \Magento\Quote\Api\Data\CartInterface
 */
class CartExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CartExtensionInterface
{
    /**
     * @return \Magento\Quote\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments()
    {
        return $this->_get('shipping_assignments');
    }

    /**
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments)
    {
        $this->setData('shipping_assignments', $shippingAssignments);
        return $this;
    }

    /**
     * @return \Amazon\Payment\Api\Data\QuoteLinkInterface|null
     */
    public function getAmazonOrderReferenceId()
    {
        return $this->_get('amazon_order_reference_id');
    }

    /**
     * @param \Amazon\Payment\Api\Data\QuoteLinkInterface $amazonOrderReferenceId
     * @return $this
     */
    public function setAmazonOrderReferenceId(\Amazon\Payment\Api\Data\QuoteLinkInterface $amazonOrderReferenceId)
    {
        $this->setData('amazon_order_reference_id', $amazonOrderReferenceId);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequireInvoice()
    {
        return $this->_get('require_invoice');
    }

    /**
     * @param string $requireInvoice
     * @return $this
     */
    public function setRequireInvoice($requireInvoice)
    {
        $this->setData('require_invoice', $requireInvoice);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTaxRegime()
    {
        return $this->_get('tax_regime');
    }

    /**
     * @param string $taxRegime
     * @return $this
     */
    public function setTaxRegime($taxRegime)
    {
        $this->setData('tax_regime', $taxRegime);
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRfc()
    {
        return $this->_get('rfc');
    }

    /**
     * @param string $rfc
     * @return $this
     */
    public function setRfc($rfc)
    {
        $this->setData('rfc', $rfc);
        return $this;
    }
}
